﻿using AdaptiveCards;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using MSTeamsTestBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MSTeamsTestBot.Bots.Dialogs
{
    public class TicketStatusDialog: ComponentDialog
    {
        private IBotServices _botServices;
        private IConfiguration Configuration;

        public TicketStatusDialog(IBotServices botServices, IConfiguration configuration) : base(nameof(TicketStatusDialog))
        {
            _botServices = botServices ?? throw new ArgumentNullException(nameof(botServices));
            Configuration = configuration;
            var waterfallSteps = new WaterfallStep[]
            {
             AskForTicketNumber,
             SummaryStep
            };
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), waterfallSteps));
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new AttachmentPrompt(nameof(AttachmentPrompt)));
            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            AddDialog(new DateTimePrompt(nameof(DateTimePrompt)));
            AddDialog(new NumberPrompt<int>(nameof(NumberPrompt<int>)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            InitialDialogId = nameof(WaterfallDialog);
        }
        private async Task<DialogTurnResult> AskForTicketNumber(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Please enter your ticket id") });
        }
        private async Task<DialogTurnResult> SummaryStep(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var ticketNumber = stepContext.Result.ToString();
            APIService aPIService = new APIService();
            var ticketData = await aPIService.GetTicketStatus(ticketNumber);
          

            if (ticketData!=null)
            {
                 var attachment=CreateAdaptiveCard.createAttachment(ticketData);
               
                var replyMessage = stepContext.Context.Activity.CreateReply();
                
                replyMessage.Attachments.Add(attachment);

                await stepContext.Context.SendActivityAsync(replyMessage);
            }
            else
            {
                await stepContext.Context.SendActivityAsync($"Unable to retrieve the results for the given ticket id.");
            }
            return await stepContext.EndDialogAsync();
        }

    }
}
