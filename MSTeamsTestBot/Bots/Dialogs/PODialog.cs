﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Extensions.Configuration;
using MSTeamsTestBot.DataModels;
using MSTeamsTestBot.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MSTeamsTestBot.Bots.Dialogs
{
    public class PODialog : ComponentDialog
    {
        private IBotServices _botServices;
        private IConfiguration Configuration;
        PORequestDataModel dataModel = new PORequestDataModel();
        public PODialog(IBotServices botServices, IConfiguration configuration) : base(nameof(PODialog))
        {
            _botServices = botServices ?? throw new ArgumentNullException(nameof(botServices));
            Configuration = configuration;
            var waterfallSteps = new WaterfallStep[]
            {
             AskForOrderNumber,
             AskForSAPName,
             AskForSAPClient,
             AskForEmail,
             SummaryStep
            };
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), waterfallSteps));
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new AttachmentPrompt(nameof(AttachmentPrompt)));
            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            AddDialog(new DateTimePrompt(nameof(DateTimePrompt)));
            AddDialog(new NumberPrompt<int>(nameof(NumberPrompt<int>)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> AskForOrderNumber(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Please enter your order number") });
        }

        private async Task<DialogTurnResult> AskForSAPName(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["Order"] = stepContext.Result.ToString();
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Please enter your SAP name") });
        }

        private async Task<DialogTurnResult> AskForSAPClient(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["SAPName"] = stepContext.Result.ToString();
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Please enter SAP client value") });
        }
        private async Task<DialogTurnResult> AskForEmail(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["SAPClient"] = stepContext.Result.ToString();
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Please enter you email id") });
        }
        private async Task<DialogTurnResult> SummaryStep(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["Email"] = stepContext.Result.ToString();
            try
            {
                PORequestDataModel dataModel = new PORequestDataModel
                {
                    OperationName = "Unlock Purchase Order",
                    Properties = new POProperties
                    {
                        Order = stepContext.Values["Order"].ToString(),
                        SapName = stepContext.Values["SAPName"].ToString(),
                        Email = stepContext.Values["Email"].ToString(),
                        SapClient = stepContext.Values["SAPClient"].ToString(),
                        Item = "Unlock Purchase Order"
                    }
                };
                APIService aPIService = new APIService();
                var responseData = await aPIService.SubmitRequetForPO(dataModel);
                await stepContext.Context.SendActivityAsync("Please wait while processing you request");
                Thread.Sleep(5000);
                var ticketData = await aPIService.GetTicketStatus(responseData.TicketId);
                if (ticketData != null)
                {
                    var attachment = CreateAdaptiveCard.createAttachment(ticketData);

                    var replyMessage = stepContext.Context.Activity.CreateReply();

                    replyMessage.Attachments.Add(attachment);

                    await stepContext.Context.SendActivityAsync(replyMessage);
                }
                else
                {
                    await stepContext.Context.SendActivityAsync($"Unable to process your request");
                }
                //await stepContext.Context.SendActivityAsync($"A ticket for your request has been created with Ticket Id {responseData.TicketId}. Kindly check with me after 5-10 mins for an update.");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return await stepContext.EndDialogAsync(null, cancellationToken);
        }

    }
}
