﻿using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime.Models;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using MSTeamsTestBot.Bots.Dialogs;
using MSTeamsTestBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MSTeamsTestBot.Bots
{
    public class MainDialog : ComponentDialog
    {
        private IBotServices _botServices;
        public IConfiguration Configuration;

        public MainDialog(IBotServices botServices, IConfiguration configuration,
           PODialog PODialog, SODialog SODialog, IDocDialog IDocDialog, TicketStatusDialog TicketStatusDialog) : base(nameof(MainDialog))
        {
            _botServices = botServices ?? throw new ArgumentNullException(nameof(botServices));
            Configuration = configuration;
            // This array defines how the Waterfall will execute.
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(PODialog);
            AddDialog(SODialog);
            AddDialog(IDocDialog);
            AddDialog(TicketStatusDialog);

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                WaitForUsersMessage,
                BeginDispatchAsync,
                FinalStepAsync,
            }));
            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }
        public override async Task<DialogTurnResult> ContinueDialogAsync(DialogContext outerDc, CancellationToken cancellationToken = default)
        {
            if (outerDc.Context.Activity.Type == ActivityTypes.Message)
            {
                var text = outerDc.Context.Activity.Text?.ToLowerInvariant();
                switch (text)
                {
                    case "quit":
                    case "cancel":
                        await outerDc.Context.SendActivityAsync("Quitting the dialog!");
                        return await outerDc.CancelAllDialogsAsync(cancellationToken);
                }
            }
            return await base.ContinueDialogAsync(outerDc, cancellationToken);
        }
        // Step #1
        private async Task<DialogTurnResult> WaitForUsersMessage(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.NextAsync(stepContext.Context.Activity.Text, cancellationToken);
        }
        // Step #2
        private async Task<DialogTurnResult> BeginDispatchAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var usersMessage = (string)stepContext.Result;
            var dispatchResult = await _botServices.Dispatch.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = dispatchResult.GetTopScoringIntent();
            return await DispatchToTopIntentAsync(stepContext, topIntent.intent, dispatchResult, cancellationToken);
        }
        // Step #3
        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.EndDialogAsync(null, cancellationToken);
        }

        //Checks for topintent and processes the appropriate dispatch method
        public async Task<DialogTurnResult> DispatchToTopIntentAsync(WaterfallStepContext stepContext, string intent, RecognizerResult dispatchResult, CancellationToken cancellationToken)
        {
            switch (intent)
            {
                // l_ConferenceRoomBookingBot l_cs-bot-luis-app-dev
                case "l_ignioDemo":
                    return await ProcessLuisAsync(stepContext, dispatchResult.Properties["luisResult"] as LuisResult, cancellationToken);
                case "q_ignioDemo":
                    return await ProcessQnAAsync(stepContext, cancellationToken);
                default:
                    await stepContext.Context.SendActivityAsync("I wasn’t able to get the result with the text you entered.");
                    return await stepContext.NextAsync(null, cancellationToken);
            }
        }

        public async Task<DialogTurnResult> ProcessLuisAsync(WaterfallStepContext stepContext, LuisResult luisResult, CancellationToken cancellationToken)
        {
            var result = luisResult.ConnectedServiceResult;
            var topIntent = result.TopScoringIntent.Intent;
            
            switch (topIntent)
            {
                case "UnlockPO":

                    return await stepContext.BeginDialogAsync(nameof(PODialog), null, cancellationToken);

                case "UnblockSO":

                    return await stepContext.BeginDialogAsync(nameof(SODialog), null, cancellationToken);

                case "RetireIDoc":

                    return await stepContext.BeginDialogAsync(nameof(IDocDialog), null, cancellationToken);

                case "TicketStatus":
                    //var entity = result.Entities?.First().Entity?.ToString();
                    //if(entity!=null)
                    //{

                    //}
                    //else
                    //{
                    //    await stepContext.Context.SendActivityAsync("Please enter your ticket number");
                        
                    //}
                    return await stepContext.BeginDialogAsync(nameof(TicketStatusDialog), null, cancellationToken);

                case "AADObjectId":
                    await stepContext.Context.SendActivityAsync($"The current user AADObject ID is {stepContext.Context.Activity.From.AadObjectId}");
                    return await stepContext.EndDialogAsync(null, cancellationToken);
                default:
                    await stepContext.Context.SendActivityAsync("I wasn’t able to get the result with the text you entered.");
                    break;
            }
            return await stepContext.NextAsync(null, cancellationToken);
        }
        private async Task<DialogTurnResult> ProcessQnAAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            try
            {
                var answers = await _botServices.QnA.GetAnswersAsync(stepContext.Context);

                if (answers.Any())
                {
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text(answers.First().Answer), cancellationToken);
                    return await stepContext.EndDialogAsync(null, cancellationToken);
                }
                else
                {
                    await stepContext.Context.SendActivityAsync("I wasn’t able to get the result with the text you entered.");
                    return await stepContext.EndDialogAsync(null, cancellationToken);
                }
            }
            catch (Exception ex)
            {

                return await stepContext.NextAsync(null, cancellationToken);
            }
        }
      
    }
}
