﻿using MSTeamsTestBot.DataModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MSTeamsTestBot.Services
{
    public  class APIService
    {
        string requestEndpoint = "https://dev2.jnjux.com/api/serverscript/apicorn_jm/savedetails?ApplicationAuthorization=Y2lwaGVye2FiVVVuVGVRRk1oU2Y2b0d1TXdPOUxnMjd1K0dNaTJZQkl2M1NHTXFkYStVSUZNK3gwVGFsRjZMQXkrdkRJanF9";
        //Username: jm_user
        //Password: Welcome@123
        public async Task<ResponseDataModel> SubmitRequetForPO(PORequestDataModel requestData)
        {

            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "am1fdXNlcjpXZWxjb21lQDEyMw==");
                var content = JsonConvert.SerializeObject(requestData);
                var response = await client.PutAsync($"{requestEndpoint}", new StringContent(content, Encoding.UTF8, "application/json"));
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseDataModel>(data);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ResponseDataModel> SubmitRequetForSO(SORequestDataModel requestData)
        {

            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "am1fdXNlcjpXZWxjb21lQDEyMw==");
                var content = JsonConvert.SerializeObject(requestData);
                var response = await client.PutAsync($"{requestEndpoint}", new StringContent(content, Encoding.UTF8, "application/json"));
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseDataModel>(data);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ResponseDataModel> SubmitRequetForIDoc(IDocRequestDataModel requestData)
        {

            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "am1fdXNlcjpXZWxjb21lQDEyMw==");
                var content = JsonConvert.SerializeObject(requestData);
                var response = await client.PutAsync($"{requestEndpoint}", new StringContent(content, Encoding.UTF8, "application/json"));
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseDataModel>(data);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<TicketResponseModel[]> GetTicketStatus(string TicketNumber)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "am1fdXNlcjpXZWxjb21lQDEyMw==");
                
                var response = await client.GetAsync($"https://dev2.jnjux.com/api/serverscript/apicorn_jm/getjobdetail?id={TicketNumber}");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TicketResponseModel[]>(data);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
