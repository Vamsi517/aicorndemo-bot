﻿using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.AI.QnA;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSTeamsTestBot.Services
{
    public class BotServices : IBotServices
    {
        // Read the setting for cognitive services (LUIS, QnA) from the appsettings.json
        public BotServices(IConfiguration configuration)
        {
            Dispatch = new LuisRecognizer(new LuisApplication(
                configuration["DispatchLuisAppId"],
                configuration["DispatchLuisAPIKey"],
                $"https://{configuration["DispatchLuisAPIHostName"]}.api.cognitive.microsoft.com"),
                new LuisPredictionOptions { IncludeAllIntents = true, IncludeInstanceData = true },
                true);

            Luis = new LuisRecognizer(new LuisApplication(
               configuration["LuisAppId"],
               configuration["LuisAPIKey"],
               $"https://{configuration["LuisAPIHostName"]}.api.cognitive.microsoft.com"),
               new LuisPredictionOptions { IncludeAllIntents = true, IncludeInstanceData = true },
               true);

            QnA = new QnAMaker(new QnAMakerEndpoint
            {
                KnowledgeBaseId = configuration["QnAKnowledgebaseId"],
                EndpointKey = configuration["QnAEndpointKey"],
                Host = configuration["QnAEndpointHostName"]
            });
        }


        public LuisRecognizer Dispatch { get; private set; }
        public LuisRecognizer Luis { get; private set; }
        public QnAMaker QnA { get; private set; }

    }
}
