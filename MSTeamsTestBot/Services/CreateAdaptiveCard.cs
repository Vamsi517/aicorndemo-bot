﻿using AdaptiveCards;
using Microsoft.Bot.Schema;
using MSTeamsTestBot.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSTeamsTestBot.Services
{
    public static class CreateAdaptiveCard
    {
        public static Attachment createAttachment(TicketResponseModel[] ticketData) 
        {
            var imageURL = "";
            if (ticketData[0].CallbackStatus.ToLower() == "success")
            {
                imageURL = "https://demodisks703.blob.core.windows.net/cognitive/success.png";
            }
            else if(ticketData[0].CallbackStatus.ToLower() == "inprogress")
            {
                imageURL = "https://demodisks703.blob.core.windows.net/cognitive/inprogress.png";
            }
            else
            {
                imageURL = "https://demodisks703.blob.core.windows.net/cognitive/failed.png";
            }

            AdaptiveCard card = new AdaptiveCard();
            card.Body.Add(new Image
            {
                Url = imageURL,
                Size=ImageSize.Medium
            });
            card.Body.Add(new TextBlock
            {
                Text = $"Ticket # : {ticketData[0].TicketId}",
                Size = TextSize.Medium,
                Weight = TextWeight.Bolder,
                HorizontalAlignment = HorizontalAlignment.Left
            });
            card.Body.Add(new TextBlock
            {
                Text = $"Status : {ticketData[0].CallbackStatus}",
                Size = TextSize.Medium,
                Weight = TextWeight.Bolder,
                HorizontalAlignment = HorizontalAlignment.Left
            });

            card.Body.Add(new TextBlock
            {
                Text = $"Message : {ticketData[0].CallbackMessage}",
                Size = TextSize.Medium,
                Weight = TextWeight.Bolder,
                HorizontalAlignment = HorizontalAlignment.Left
            });

            Attachment attachment = new Attachment()
            {
                ContentType = AdaptiveCard.ContentType,
                Content = card
            };
            return attachment;
        }
    }
}
