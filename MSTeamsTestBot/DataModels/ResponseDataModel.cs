﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSTeamsTestBot.DataModels
{
    public class ResponseDataModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("TicketId")]
        public string TicketId { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("CallbackStatus")]
        public string CallbackStatus { get; set; }
    }
}
