﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSTeamsTestBot.DataModels
{
    public class TicketResponseModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public string UpdatedAt { get; set; }

        [JsonProperty("updatedBy")]
        public string UpdatedBy { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("OperationName")]
        public string OperationName { get; set; }

        [JsonProperty("Properties")]
        public string Properties { get; set; }

        [JsonProperty("TicketId")]
        public string TicketId { get; set; }

        [JsonProperty("Status")]
        public string Status { get; set; }

        [JsonProperty("CallbackStatus")]
        public string CallbackStatus { get; set; }

        [JsonProperty("CallbackMessage")]
        public string CallbackMessage { get; set; }
    }
    public class ResposeModel
    {
        public TicketResponseModel[] responseModels { get; set; }
    }
}
