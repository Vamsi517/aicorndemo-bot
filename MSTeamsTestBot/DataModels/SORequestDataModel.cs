﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSTeamsTestBot.DataModels
{
    public class SORequestDataModel
    {
        [JsonProperty("OperationName")]
        public string OperationName { get; set; }

        [JsonProperty("Properties")]
        public SOProperties Properties { get; set; }
    }
    public partial class SOProperties
    {
        [JsonProperty("item")]
        public string Item { get; set; }

        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("sapName")]
        public string SapName { get; set; }

        [JsonProperty("sapClient")]
        public string SapClient { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
