﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSTeamsTestBot.DataModels
{
    public class PORequestDataModel
    {
        [JsonProperty("OperationName")]
        public string OperationName { get; set; }

        [JsonProperty("Properties")]
        public POProperties Properties { get; set; }
    }

    public partial class POProperties
    {
        [JsonProperty("item")]
        public string Item { get; set; }

        [JsonProperty("order")]
        public string Order { get; set; }

        [JsonProperty("sapName")]
        public string SapName { get; set; }

        [JsonProperty("sapClient")]
        public string SapClient { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }

}
